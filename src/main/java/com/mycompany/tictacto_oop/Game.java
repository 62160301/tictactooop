/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tictacto_oop;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class Game {

  private  Player playerX;
  private  Player playerO;
  private  Player turn;
  private  Table table;
  private  int row, col;
  private  char choice;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void showBye() {
        System.out.println("Bye bye ...");
    }

    public void newGame() {
        table = new Table(playerX, playerO);
        table.switchPlayer();
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (checkFinish()) {
                break;
            }
            table.switchPlayer();
        }

    }

    public void showScore() {
        System.out.println("Total score!!");
        System.out.println("Win plyer X : " + playerX.getWin());
        System.out.println("Lose plyer X : " + playerX.getLose());
        System.out.println("Win plyer O : " + playerO.getWin());
        System.out.println("Lose plyer O : " + playerO.getLose());
        System.out.println("Draw : " + playerO.getDraw());
    }

    private boolean checkFinish() {
        if (table.isFinish()) {
            if (table.getWinner() == null) {
                System.out.println("Draw!!");
            } else {
                System.out.println(table.getWinner().getName() + " Win!!!");
            }
            System.out.println("Y Play again | N Exit");
            choice = kb.next().charAt(0);
            if (choice == 'Y') {
                table.setTurn();
                newGame();
            } else {
                this.showScore();
                this.showBye();
                return true;
            }
        }
        return false;
    }
}
